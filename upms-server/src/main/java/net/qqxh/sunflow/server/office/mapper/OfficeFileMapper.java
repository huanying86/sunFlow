package net.qqxh.sunflow.server.office.mapper;

import net.qqxh.sunflow.mapper.SuperMapper;
import net.qqxh.sunflow.server.office.bean.OfficeFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * Copyright (C), 2019-2020, sunflow开发团队
 * 在线编辑文件 Mapper接口
 *
 * @fileName OfficeFileMapper.java
 * @date     2019/5/25 15:36
 * @author   cjy
 */
@Mapper
public interface OfficeFileMapper extends SuperMapper<OfficeFile> {
}
